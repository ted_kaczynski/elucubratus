#!/bin/bash
set -e
shopt -s extglob nullglob

if [[ $# == 0 ]]; then
    echo "usage: $0 <package>"
    exit
fi

export PKG_MAKE=$0
export PKG_NAME=${1%_}
export PKG_BASE=$(realpath "$(dirname "$0")")

. "${PKG_BASE}/helper.sh"

if [[ ! -x ${PKG_BASE}/util/arid || ${PKG_BASE}/util/arid -ot ${PKG_BASE}/util/arid.cpp ]]; then
    g++ -I ~/menes -o "${PKG_BASE}"/util/arid{,.cpp}
fi

export CODESIGN_ALLOCATE="$(which codesign_allocate)"

for DEP_NAME in "${PKG_DEPS[@]}"; do
    "${PKG_MAKE}" "${DEP_NAME}"
done

export PKG_HASH=$({
    "${PKG_BASE}"/util/catdir.sh "${PKG_DATA}" -L \( -name '.svn' -o -name '_*' \) -prune -o

    for DEP_NAME in "${PKG_DEPS[@]}"; do
        "${PKG_BASE}"/util/catdir.sh "$(PKG_DEST_ "${DEP_NAME}")"
        DEP_MORE="$(PKG_MORE_ "${DEP_NAME}")"
        if [[ -d ${DEP_MORE} ]]; then
            "${PKG_BASE}"/util/catdir.sh "${DEP_MORE}"
        fi
    done
} | md5sum | cut -d ' ' -f 1)

echo "hashed data ${PKG_NAME} to: ${PKG_HASH}"

if [[ -e "${PKG_STAT}/data-md5" && ${PKG_HASH} == $(cat "${PKG_STAT}/data-md5") ]]; then
    echo "skipping re-build of ${PKG_NAME}"
    exit
fi

mkdir -p "${PKG_STAT}"
rm -f "${PKG_STAT}/data-md5"

rm -rf "${PKG_MORE}"
mkdir -p "${PKG_MORE}"

rm -rf "${PKG_DEST}"
mkdir -p "${PKG_DEST}"

rm -rf "${PKG_WORK}"
mkdir -p "${PKG_WORK}"

function pkg:patch() {
    pkg:libtool_ libtool
    pkg:libtool_ ltmain.sh

    if [ -f "debian/patches/series" ]; then
        for diff in $(cut -d'#' -f1 < debian/patches/series); do
            echo "patching with debian ${diff}..."
            patch ${args:=-p1} < "debian/patches/${diff}"
        done
    fi

    for diff in "${PKG_DATA}"/*.diff; do
        if [[ ${diff} =~ .*/_[^/]*.diff$ ]]; then
            continue;
        fi

        args=$(cat ${diff%.diff}.args 2>/dev/null || true)
        echo "patching with ${diff}..."
        patch ${args:=-p1} <"${diff}"
    done

    for diff in "${PKG_DATA}"/*.diff."${PKG_ARCH}"; do
        if [[ ${diff} =~ .*/_[^/]*.diff.${PKG_ARCH}$ ]]; then
            continue;
        fi

        args=$(cat ${diff%.diff}.args 2>/dev/null || true)
        echo "patching with ${diff}..."
        patch ${args:=-p1} <"${diff}"
    done
}

export -f pkg:patch

function pkg:bin() {
    if [[ $# -eq 0 ]]; then
        pushd "${PKG_DEST}/usr/bin"
        set $(ls)
        popd
    fi

    ${FAKEROOT} mkdir -p "${PKG_DEST}/bin"
    for bin in "$@"; do
        ${FAKEROOT} mv -v "${PKG_DEST}/usr/bin/${bin}" "${PKG_DEST}/bin/${bin}"
    done

    ${FAKEROOT} rmdir --ignore-fail-on-non-empty -p "${PKG_DEST}/usr/bin"
}

export -f pkg:bin

function pkg:autoconf() {
    for m4 in $(find -name "*.m4"); do
        patch -F 3 -r/dev/null "${m4}" "${PKG_BASE}/util/libtool.m4.diff" || true
    done

    autoconf
}

export -f pkg:autoconf

export PKG_CONF=./configure

function pkg:libtool_() {
    for ltmain in $(find -name "$1"); do
        patch -r/dev/null "${ltmain}" "${PKG_BASE}/util/libtool.diff" || true
    done
}

export -f pkg:libtool_

function pkg:setup() {
    pkg:extract
    if [ -z ""*/ ]; then
        echo "Empty package??"
        exit 1
    fi
    if [ -d "debian" ]; then
        mv debian "$(ls | grep -v debian)"
    fi
    cd */
    pkg:patch
}

export -f pkg:setup

function pkg:configure() {
    cfg=("${PKG_CONF}" \
        ac_cv_prog_cc_g=no ac_cv_prog_cxx_g=no \
        --build="$(${PKG_BASE}/util/config.guess)" \
        --host="${PKG_TARG}" \
        --enable-static=no \
        --enable-shared=yes \
        --prefix=$(cat "${PKG_BASE}/arch/${PKG_ARCH}/${PKG_CFTARG}/prefix") \
        --localstatedir="/var/cache/${PKG_NAME}" \
        CFLAGS="-O2 ${PKG_MCPU} ${CFLAGS}" \
        CXXFLAGS="-O2 ${PKG_MCPU} ${CXXFLAGS}" \
        "$@")
    echo "${cfg[@]}"
    PKG_CONFIG="$(realpath "${PKG_BASE}/util/pkg-config.sh")" \
    "${cfg[@]}"
}

export -f pkg:configure

function pkg:make() {
    make -j16 CC="${PKG_TARG}-gcc" CXX="${PKG_TARG}-g++" AR="${PKG_TARG}-ar" "$@"
}

export -f pkg:make

function pkg:install() {
    ${FAKEROOT} make install -j16 CC="${PKG_TARG}-gcc" CXX="${PKG_TARG}-g++" AR="${PKG_TARG}-ar" DESTDIR="${PKG_DEST}" "$@"
    # Go die in a horrible fire, libtool
    find "${PKG_DEST}" -name '*.la' -exec rm {} +
}

export -f pkg:install

function pkg:extract() {
    for tgz in "${PKG_DATA}"/*.{tar.{gz,xz,bz2,lz,lzma},tgz}; do
        tar -xf "${tgz}"
    done

    for zip in "${PKG_DATA}"/*.zip; do
        unzip "${zip}"
    done
}

export -f pkg:extract

function pkg:usrbin() {
    pkg: mkdir -p /usr/bin
    pkg: cp -a "$@" /usr/bin
}

export -f pkg:usrbin

function subpkg:stage_() {
    shopt -s nullglob extglob
    export PKG_PARENT="${PKG_NAME}"
    for install in "${PKG_DATA}"/*.install; do
        PKG_NAME="$(basename ${install} .install)"
        . "${PKG_BASE}"/helper.sh
        echo Staging subproject ${PKG_NAME}
        IFS=$'\n' read -r -d '' -a PKG_DEB_GLOBS < ${install}
        rm -rf "${PKG_DEST}"
        for pkg_deb_glob in "${PKG_DEB_GLOBS[@]}"; do
            for pkg_deb_file in "${PKG_PARENT_DEST}/"${pkg_deb_glob}; do
                SRC_FILE="${pkg_deb_file}"
                DST_FILE="${PKG_DEST}/${SRC_FILE/#${PKG_PARENT_DEST}*(\/)/}"
                if [ -d "${pkg_deb_file}" ]; then
                    SRC_DIR="${SRC_FILE}"
                    DST_DIR="${DST_FILE}"
                    install -dvp "${SRC_DIR}" "${DST_DIR}"
                    cp -va "${SRC_DIR}"/. "${DST_DIR}"
                else
                    SRC_DIR="$(dirname ${SRC_FILE})"
                    DST_DIR="$(dirname ${DST_FILE})"
                    if [ ! -d "${DST_DIR}" ]; then
                        install -dvp "${SRC_DIR}" "${DST_DIR}"
                    fi
                    cp -va "${SRC_FILE}" "${DST_FILE}"
                fi
            done
        done
        echo Staged subproject ${PKG_DEB_NAME}
    done
    echo done
}

export -f subpkg:stage_

function subpkg:stage() {
    ${FAKEROOT} bash -c "subpkg:stage_"
}

export -f subpkg:stage

cd "${PKG_WORK}"
"${PKG_BASE}/exec.sh" "${PKG_NAME}" . "${PKG_DATA}/make.sh"

function rmdir_() {
    if [[ -d "$1" ]]; then
        rmdir --ignore-fail-on-non-empty "$1"
    fi
}

rm -rf "${PKG_DEST}/usr/share/man"
rm -rf "${PKG_DEST}/usr/share/info"
rm -rf "${PKG_DEST}/usr/share/gtk-doc"
rm -rf "${PKG_DEST}/usr/share/doc"
rm -rf "${PKG_DEST}/usr/share/locale"
rm -rf "${PKG_DEST}/usr/man"
rm -rf "${PKG_DEST}/usr/local/share/man"
rm -rf "${PKG_DEST}/usr/local/OpenSourceVersions"
rm -rf "${PKG_DEST}/usr/local/OpenSourceLicenses"
rm -f "${PKG_DEST}/usr/lib/charset.alias"
rm -rf "${PKG_DEST}/usr/info"
rm -rf "${PKG_DEST}/usr/docs"
rm -rf "${PKG_DEST}/usr/doc"

rmdir_ "${PKG_DEST}/usr/share"
rmdir_ "${PKG_DEST}/usr/local/share"
rmdir_ "${PKG_DEST}/usr/local"
rmdir_ "${PKG_DEST}/usr/lib"
rmdir_ "${PKG_DEST}/usr"

if grep -r "${PKG_BASE}" "${PKG_DEST}"; then
    echo "WARNING - files contain ${PWD}"
fi

if [[ -e "${PKG_BASE}/arch/${PKG_ARCH}/${PKG_CFTARG}/strip" ]]; then
    . "${PKG_BASE}/arch/${PKG_ARCH}/${PKG_CFTARG}/strip"
fi

find "${PKG_DEST}" -type f -name '*.elc' -print0 | while read -r -d $'\0' bin; do
    sed -i -e '
        s/^;;; Compiled by .*$//
        s/^;;; from file .*$//
        s/^;;; in Emacs version .*$//
        s/^;;; with .*$//
    ' "${bin}"
done

find "${PKG_DEST}" -type f -name '*.a' -print0 | while read -r -d $'\0' bin; do
    chmod u+w "${bin}"
    "${PKG_BASE}/util/arid" "${bin}"
done

if [[ -f "${PKG_STAT}/data-ver" && -f "${PKG_STAT}/dest-ver" ]] && dpkg --compare-versions $(cat "${PKG_DATA}/_metadata/version") gt $(cat "${PKG_STAT}/data-ver"); then
    rm "${PKG_STAT}/dest-ver"
fi
cp -aL "${PKG_DATA}/_metadata/version" "${PKG_STAT}/data-ver"
echo "${PKG_HASH}" >"${PKG_STAT}/data-md5"

echo "hashed code ${PKG_NAME} to: $("${PKG_BASE}"/util/catdir.sh "${PKG_DEST}" | md5sum | cut -d ' ' -f 1)"
