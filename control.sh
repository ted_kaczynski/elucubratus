#!/bin/bash
shopt -s extglob nullglob

PKG_NAME=$1
shift
if [[ ${PKG_NAME} == */* ]]; then
    PKG_PARENT="${PKG_NAME%%/*}"
    PKG_NAME="${PKG_NAME#*/}"
fi
export PKG_NAME PKG_PARENT

export PKG_BASE=$(realpath "$(dirname "$0")")
. "${PKG_BASE}/helper.sh"

if [[ -n $2 ]]; then
    PKG_VRSN=$2
fi

cat <<EOF
Package: ${PKG_NAME}
EOF

if [[ ${PKG_PRIO} == +* ]]; then
    cat <<EOF
Essential: yes
EOF
fi

if [[ $1 == status ]]; then
    cat <<EOF
Status: install ok installed
EOF
fi

cat <<EOF
Priority: ${PKG_PRIO#+}
Section: $(cat "${PKG_FILE_SECTION}")
EOF

if [[ $1 == status || $1 == available ]]; then
    cat <<EOF
Installed-Size: $(dpkg -f "${PKG_BASE}/debs/${PKG_CFTARG}/${PKG_NAME}_${PKG_VRSN}-${PKG_RVSN}_${PKG_DEB_ARCH}.deb" Installed-Size)
EOF
elif [[ $1 == control ]]; then
    cat <<EOF
Installed-Size: $(du -s "${PKG_DEST}" | cut -d $'\t' -f 1)
EOF
fi

cat <<EOF
Maintainer: $(cat "${PKG_FILE_MAINTAINER}")
Architecture: ${PKG_ARCH}
EOF

echo -n "Version: ${PKG_VRSN}"

if [[ $1 == status || $1 == available ]]; then
    echo "-${PKG_RVSN}"
else
    echo
fi

if [[ $1 == available ]]; then
    cat <<EOF
Size: $(find "${PKG_DEST}" -type f -exec cat {} \; | gzip -c | wc -c | cut -d $'\t' -f 1)
EOF
fi

if [[ -e ${PKG_FILE_PREDEPENDS_} ]]; then
    echo "Pre-Depends: $(cat "${PKG_FILE_PREDEPENDS_}")"
else
    unset comma

    if [[ ${PKG_ZLIB} == lzma ]]; then
        if [[ ${comma+@} == @ ]]; then
            echo -n ","
        else
            echo -n "Pre-Depends:"
            comma=
        fi

        echo -n " dpkg (>= 1.14.25-8)"
    fi

    if [[ -e ${PKG_FILE_PREDEPENDS} ]]; then
        if [[ ${comma+@} == @ ]]; then
            echo -n ","
        else
            echo -n "Pre-Depends:"
            comma=
        fi

        echo -n " $(cat "${PKG_FILE_PREDEPENDS}")"
    fi

    if [[ ${comma+@} == @ ]]; then
        echo
    fi
fi

if [[ ! -e ${PKG_FILE_DEPENDS_} ]]; then
    . "${PKG_BASE}/autodeps.sh"
    unset comma
    for dep in "${PKG_DEPS[@]}"; do
        if [[ ${dep} == _* ]]; then
            continue
        fi

        if [[ ${comma+@} == @ ]]; then
            echo -n ","
        else
            echo -n "Depends:"
            comma=
        fi

        echo -n " $(basename "${dep}" .dep)"
        
        ver=${PKG_DATA}/_metadata/${dep%.dep}.ver
        if [[ -e "${ver}" ]]; then
            echo -n " (>= $(cat "${ver}"))"
        fi
    done

    if [[ -e ${PKG_FILE_DEPENDS} ]]; then
        if [[ ${comma+@} == @ ]]; then
            echo -n ","
        else
            echo -n "Depends:"
            comma=
        fi

        echo -n " $(cat "${PKG_FILE_DEPENDS}" | sed -e s/%MYVERSION%/${PKG_VRSN%-*}/g)"
    fi

    if [[ -e ${PKG_FILE_DEPENDS_ARCH} ]]; then
        if [[ ${comma+@} == @ ]]; then
            echo -n ","
        else
            echo -n "Depends:"
            comma=
        fi

        echo -n " $(cat "${PKG_FILE_DEPENDS_ARCH}" | sed -e s/%MYVERSION%/${PKG_VRSN%-*}/g)"
    fi

    if [[ ${comma+@} == @ ]]; then
        echo
    fi
elif [[ -s ${PKG_DATA}/_metadata/depends_ ]]; then
    echo "Depends: $(cat "${PKG_DATA}/_metadata/depends_")"
fi

if [[ -e ${PKG_FILE_REPLACES} ]]; then
    cat <<EOF
Replaces: $(cat "${PKG_FILE_REPLACES}")
EOF
fi

if [[ -e ${PKG_FILE_CONFLICTS} ]]; then
    cat <<EOF
Conflicts: $(cat "${PKG_FILE_CONFLICTS}")
EOF
fi

if [[ -e ${PKG_FILE_BREAKS} ]]; then
    cat <<EOF
Breaks: $(cat "${PKG_FILE_BREAKS}")
EOF
fi

if [[ -e ${PKG_FILE_PROVIDES} ]]; then
    cat <<EOF
Provides: $(cat "${PKG_FILE_PROVIDES}" | sed -e s/%MYVERSION%/${PKG_VRSN%-*}/)
EOF
fi

PKG_SHORT_DESCRIPTION="$(head -n 1 "${PKG_FILE_DESCRIPTION}")"
if [ -z "${PKG_SHORT_DESCRIPTION}" ]; then
    echo "Error: need a description to make a package" >&2
    exit 1;
fi

cat <<EOF
Description: ${PKG_SHORT_DESCRIPTION}
EOF

if [[ $(wc -l "${PKG_FILE_DESCRIPTION}" | cut -d ' ' -f 1) -gt 1 ]]; then
    cat <<EOF
$(tail -n +2 "${PKG_FILE_DESCRIPTION}" | fold -sw 72 | sed -e 's/^/ /')
EOF
fi

if [[ -e ${PKG_FILE_NAME} ]]; then
    cat <<EOF
Name: $(cat "${PKG_FILE_NAME}")
EOF
fi

if [[ -e ${PKG_FILE_AUTHOR} ]]; then
    cat <<EOF
Author: $(cat "${PKG_FILE_AUTHOR}")
EOF
fi

if [[ -e ${PKG_FILE_HOMEPAGE} ]]; then
    cat <<EOF
Homepage: $(cat "${PKG_FILE_HOMEPAGE}")
EOF
fi

if [[ -e ${PKG_FILE_DEPICTION} ]]; then
    cat <<EOF
Depiction: $(cat "${PKG_FILE_DEPICTION}")
EOF
fi

if [[ $1 == status || $1 == available ]]; then
    echo
fi
