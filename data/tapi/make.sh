pkg: mkdir -p /usr/bin
for bin in tapi tapi-frontend tapi-binary-reader tapi-import tapi-run; do
    pkg: ln -s ../lib/llvm-10/bin/${bin} /usr/bin/${bin}
done
