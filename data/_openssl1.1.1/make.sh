pkg:setup
case ${PKG_TARG} in
    aarch64-*)
        TARGET="iphoneos-arm64" ;;
    arm*)
        TARGET="iphoneos-arm32" ;;
    *)
        echo "Unknown PKG_TARG: " ${PKG_TARG} ;;
esac

echo ./Configure -D__DARWIN_UNIX03 ${TARGET} --prefix=/usr --openssldir=/usr/lib/ssl shared
./Configure -D__DARWIN_UNIX03 ${TARGET} --prefix=/usr --openssldir=/usr/lib/ssl shared
pkg:make
make install_sw install_ssldirs DESTDIR="${PKG_DEST}"
pkg: rm -rf /usr/lib/man /usr/lib/ssl/man
pkg: mkdir -p /etc/ssl
mv "${PKG_DEST}"/usr/lib/ssl/{certs,openssl.cnf,private} "${PKG_DEST}"/etc/ssl
ln -s /etc/ssl/{certs,openssl.cnf,private} "${PKG_DEST}"/usr/lib/ssl/
rm -rf "${PKG_DEST}"/usr/lib/*.a "${PKG_DEST}"/etc/ssl/*.dist
