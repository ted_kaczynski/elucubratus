pkg:setup
autoreconf -f -i
pkg:configure --without-cython CC="${PKG_TARG}-gcc" CXX="${PKG_TARG}-g++"
pkg:make V=1
pkg:install
subpkg:stage
