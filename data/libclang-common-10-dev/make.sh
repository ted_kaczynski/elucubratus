pkg: mkdir -p /usr/lib/llvm-10/lib/clang /usr/lib/clang
cp -a "$(PKG_DEST_ _llvm10)"/usr/lib/llvm-10/lib/clang/* "${PKG_DEST}"/usr/lib/llvm-10/lib/clang/
FILENAME="$(basename "${PKG_DEST}"/usr/lib/llvm-10/lib/clang/10*/)"
pkg: ln -s ../llvm-10/lib/clang/${FILENAME} /usr/lib/clang/
