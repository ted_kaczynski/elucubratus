pkg:setup
"${PKG_TARG}-gcc" -dynamiclib -O2 -Ifbsdcompat -Idarwin -install_name /usr/lib/libmissing.dylib -o libmissing.dylib stdlib/FreeBSD/system.c
pkg: mkdir -p /usr/lib /usr/include
pkg: cp libmissing.dylib /usr/lib/
pkg: cp %/missing.h /usr/include/
