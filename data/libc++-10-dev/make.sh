pkg: mkdir -p /usr/lib/llvm-10/{include/c++,lib}
cp -a "$(PKG_DEST_ _llvm10)"/usr/lib/llvm-10/include/c++ "${PKG_DEST}"/usr/lib/llvm-10/include/
cp -a "$(PKG_DEST_ _llvm10)"/usr/lib/llvm-10/lib/libc++.dylib "${PKG_DEST}"/usr/lib/llvm-10/lib/
cp -a "$(PKG_DEST_ _llvm10)"/usr/lib/llvm-10/lib/libc++.1.dylib "${PKG_DEST}"/usr/lib/llvm-10/lib/
