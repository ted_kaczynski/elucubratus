pkg:setup
autoreconf -fi
CFLAGS="-D_LARGEFILE_SOURCE -std=gnu89" pkg:configure --enable-shared --disable-static
pkg:make
pkg:install
subpkg:stage
