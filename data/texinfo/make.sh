export PERL_SRC="$(PKG_DEST_ perl)"
export PERL="$(PKG_WORK_ perl)"/perl-$(cat $(PKG_DATA_ perl)/_metadata/version)/miniperl_top
pkg:setup
pkg:configure
make -j8
pkg:install
