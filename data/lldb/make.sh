pkg: mkdir -p /usr/bin
for bin in lldb lldb-argdumper lldb-instr lldb-server lldb-vscode; do
    pkg: ln -s ../lib/llvm-10/bin/${bin} /usr/bin/${bin}
done
