pkg: mkdir -p /usr/lib
cp -a "$(PKG_DEST_ _openssl1.1.1)"/etc "${PKG_DEST}"/
cp -a "$(PKG_DEST_ _openssl1.1.1)"/usr/bin "${PKG_DEST}"/usr
cp -a "$(PKG_DEST_ _openssl1.1.1)"/usr/lib/ssl "${PKG_DEST}"/usr/lib
rm "${PKG_DEST}"/usr/lib/ssl/*.dist
