pkg: mkdir -p /usr/lib /usr/bin
cp -a "$(PKG_DEST_ _libxslt)"/usr/bin/xslt-config "${PKG_DEST}"/usr/bin/
cp -a "$(PKG_DEST_ _libxslt)"/usr/lib/{pkgconfig,xsltConf.sh} "${PKG_DEST}"/usr/lib/
cp -a "$(PKG_DEST_ _libxslt)"/usr/{include,share} "${PKG_DEST}"/usr
