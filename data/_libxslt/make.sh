pkg:setup
cd libxslt
NOCONFIGURE=1 ./autogen.sh
# XXX: fix libgcrypt-config call to add --prefix
pkg:configure --with-python=no --with-libxml-prefix="$(PKG_DEST_ _libxml2)/usr" --with-libxml-include-prefix="$(PKG_DEST_ _libxml2)/usr/include/libxml2" ac_cv_path_LIBGCRYPT_CONFIG=no
pkg:make
cp "../Pregenerated Files/xslt-config" xslt-config
pkg:install
rm -f "${PKG_DEST}/usr/lib/"*.dylib
