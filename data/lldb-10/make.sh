pkg: mkdir -p /usr/lib/llvm-10/bin
cp -a "$(PKG_DEST_ _llvm10)"/usr/lib/llvm-10/bin/lldb* "${PKG_DEST}"/usr/lib/llvm-10/bin/

pkg: mkdir -p /usr/bin
for bin in "${PKG_DEST}/usr/lib/llvm-10/bin"/*; do
    pkg: ln -s ../lib/llvm-10/bin/$(basename "${bin}") /usr/bin/$(basename "${bin}")-10
done
