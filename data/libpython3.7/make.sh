pkg: mkdir -p /usr
LIBNAME=${PKG_NAME#lib}
cp -a "$(PKG_DEST_ _${LIBNAME})"/usr/lib "${PKG_DEST}"/usr/
rm -rf "${PKG_DEST}"/usr/lib/{pkgconfig,python*/config*/}
