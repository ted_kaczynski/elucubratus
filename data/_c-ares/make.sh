pkg:setup
pkg:configure \
    curl_cv_func_recv_args="int,void *,size_t,int,ssize_t" \
    curl_cv_func_recvfrom_args="int,void *,size_t,int,struct sockaddr *,socklen_t *,ssize_t" \
    curl_cv_func_send_args="int,const void *,size_t,int,ssize_t"
pkg:make
pkg:install

# Not splited into lib,-dev packages because no bins.
