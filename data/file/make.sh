shopt -s extglob
pkg:setup
cd ..
mv * target
pkg:extract
mv !(target) host
cd host
./configure
make -j16
cd ../target
pkg:configure
make -j16 FILE_COMPILE="$(pwd)"/../host/src/file 
pkg:install
