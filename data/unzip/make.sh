pkg:extract
cd unzip*
for patch in $(cat ../debian/patches/series); do
    patch -p1 < ../debian/patches/${patch}
done
pkg:patch
cp unix/Makefile .
pkg:make unzips CC=${PKG_TARG}-gcc \
    CF='-O3 -Wall -I. -DBSD -DUNIX -DACORN_FTYPE_NFS -DWILD_STOP_AT_DIR \
    -DLARGE_FILE_SUPPORT -DUNICODE_SUPPORT -DUNICODE_WCHAR -DUTF8_MAYBE_NATIVE \
    -DNO_LCHMOD -DDATE_FORMAT=DF_YMD -DUSE_BZIP2 -DIZ_HAVE_UXUIDGID ' \
    LF2= L_BZ2=-lbz2

pkg:usrbin unzip funzip unzipsfx
